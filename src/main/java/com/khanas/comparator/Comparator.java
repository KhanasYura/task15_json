package com.khanas.comparator;

import com.khanas.model.Device;

import java.util.Arrays;
import java.util.List;

public class Comparator {

    public static List<Device> deviceSorted(final Device[] devices) {
        java.util.Comparator<Device> comparator =
                (o1, o2) -> o1.getName().compareTo(o2.getName());
        Arrays.sort(devices, comparator);
        return Arrays.asList(devices);
    }
}
