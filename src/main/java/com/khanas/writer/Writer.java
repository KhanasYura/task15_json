package com.khanas.writer;

import com.google.gson.GsonBuilder;
import com.khanas.model.Device;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

public class Writer {

    public static void writeToJson(final List<Device> devices,
                                   final File json) {
        try (java.io.Writer writer = new FileWriter(json)) {
            new GsonBuilder().create().toJson(devices, writer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
