package com.khanas.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import com.github.fge.jsonschema.report.ProcessingReport;

import java.io.File;
import java.io.IOException;


public class Validator {

    public static boolean validate(final File json, final File jsonSchema)
            throws IOException, ProcessingException {
        JsonNode data = JsonLoader.fromFile(json);
        JsonNode schema = JsonLoader.fromFile(jsonSchema);

        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonValidator validator = factory.getValidator();
        ProcessingReport report = validator.validate(schema, data);

        return report.isSuccess();
    }
}
