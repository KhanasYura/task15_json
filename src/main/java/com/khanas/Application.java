package com.khanas;

import com.github.fge.jsonschema.exceptions.ProcessingException;
import com.khanas.model.Device;
import com.khanas.parser.GsonParser;
import com.khanas.parser.JacksonParser;
import com.khanas.validator.Validator;
import com.khanas.writer.Writer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Application {

    private static Logger logger = LogManager.getLogger();

    public static void main(final String[] args)
            throws IOException, ProcessingException {
        File json = new File("src/main/resources/Devices.json");
        File schema = new File("src/main/resources/DevicesSchema.json");
        File gsonFile = new File("src/main/resources/Gson.json");
        File jacksonFile = new File("src/main/resources/Jackson.json");
        if (Validator.validate(json, schema)) {
            logger.info("------------Gson Parser-------------");
            GsonParser gsonParser = new GsonParser();
            List<Device> devices = gsonParser.getDevices(json);
            printDevices(devices);
            Writer.writeToJson(devices, gsonFile);
            logger.info("----------Jackson Parser------------");
            JacksonParser jacksonParser = new JacksonParser();
            devices = jacksonParser.getDevices(json);
            printDevices(devices);
            Writer.writeToJson(devices, jacksonFile);
        } else {
            logger.error("JSON file is not valid");
        }
    }

    public static void printDevices(final List<Device> devices) {
        for (Device device : devices) {
            logger.info(device.toString());
        }
    }
}
