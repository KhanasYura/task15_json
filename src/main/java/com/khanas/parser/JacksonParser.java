package com.khanas.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.khanas.comparator.Comparator;
import com.khanas.model.Device;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class JacksonParser {

    private ObjectMapper objMapper;
    private Device[] devices;

    public JacksonParser() {
        this.objMapper = new ObjectMapper();
        devices = new Device[0];
    }

    public final List<Device> getDevices(final File json) throws IOException {
        devices = objMapper.readValue(json, Device[].class);
        return Comparator.deviceSorted(devices);
    }


}
