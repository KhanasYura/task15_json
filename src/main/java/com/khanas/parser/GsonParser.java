package com.khanas.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.khanas.comparator.Comparator;
import com.khanas.model.Device;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class GsonParser {
    private Gson gson;
    private Device[] devices;

    public GsonParser() {
        this.gson = new GsonBuilder().create();
        this.devices = new Device[0];
    }

    public final List<Device> getDevices(final File json) throws FileNotFoundException {
        devices = gson.fromJson(new FileReader(json), Device[].class);
        return Comparator.deviceSorted(devices);
    }

}
